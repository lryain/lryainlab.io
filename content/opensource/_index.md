---
title: Open Source Contributions
date: 2023-03-18T16:49:15.046Z
---

A collection of efforts to which I contributed, but did not create. Contributing back to Open Source projects is a strong passion of mine, and requires a considerate approach to learn norms, standards and approach for each community for a successful merge!
