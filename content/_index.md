---
title: "About"
---

### Hi there 👋 I'm Hu Liu. A rust programming enthusiasts and game developers.

- 🌱 I’m currently learning rust and [game](https://hyperworld.cc) dev.
- 💬 Ask me anything [here](https://github.com/lryain/lryain/issues).
- 📫 How to reach me: [Mail](mailto:47129927@qq.com).
- 🐯 My GitHub [Overview](https://github.com/lryain).

[![Stat](https://github-readme-stats.vercel.app/api?username=lryain&count_private=true&show_icons=true&line_height=20&theme=default)](https://github.com/lryain)
[![Top-Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=lryain&layout=compact&hide=HTML,PostScript&theme=default_repocard)](https://github.com/lryain)